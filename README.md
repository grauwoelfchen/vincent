# Vincent

```txt
Vincent; Vpn Installation Note and Configuration on gENToo linux
```

This project is a note about `OpenVPN` setup on Gentoo Linux, which
includes templates for your client users.

```zsh
# uname -a
Linux <HOSTNAME> 4.8.6-x86_64-linode78 #1 SMP Tue Nov 1 14:51:21 EDT 2016 x86_64 Intel(R) Xeon(R) CPU E5-2697 v4 @ 2.30GHz GenuineIntel GNU/Linux
```


## Note(s)

* [Setup Documentation](doc/index.md)
  * [Server](doc/server.md)
  * [Client](doc/client.md)

See also [Innocent](
https://gitlab.com/grauwoelfchen/innocent) for general Gentoo Linux Instance
setup.


## Quick guide

### How to add new client

See sample `rsa/john.smith` directory.

```zsh
% cd /path/to/easy-rsa
% ./easyrsa build-client-full <CLIENT>

Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
........+++
........+++
writing new private key to '/home/<USER>/easy-rsa/pki/private/<CLIENT>.key.NNN'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
Using configuration from /home/<USER>/easy-rsa/openssl-1.0.cnf
Enter pass phrase for /home/<USER>/easy-rsa/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
countryName           :PRINTABLE:'...'
stateOrProvinceName   :ASN.1 12:'...'
localityName          :ASN.1 12:'...'
organizationName      :ASN.1 12:'...'
organizationalUnitName:ASN.1 12:'...'
commonName            :ASN.1 12:'<CLIENT>'
emailAddress          :IA5STRING:'...'
Certificate is to be certified until Jan 26 10:56:16 2027 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated
```

### Use template

```zsh
% cd /path/to/vincent
% cp -R rsa/john.smith <CLIENT>

# ca.crt
% scp <SERVER>:/path/to/easy-rsa/pki/ca.crt pki/ca.crt

# client crt/key
% scp <SERVER>:/path/to/easy-rsa/pki/issued/<CLIENT>.key pki/issued
<CLIENT>.crt
% scp <SERVER>:/path/to/easy-rsa/pki/private/<CLIENT>.key pki/private
<CLIENT>.key

```

And then, update/rename `john.smith/example.opvn` for user setup.



## Links

* https://openvpn.net/index.php/open-source/documentation.html


## License

```
Vincent
Copyright (c) 2018 Yasuhiro Asaka
```

### Scripts

The scripts into `lib` directory  are distributed as
**GNU General Public License** (version 3)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Notes

The text files are distributed as **GNU Free Documentation License**.
(version 1.3)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

See [LICENSE](LICENSE). (`GFDL-1.3`)
