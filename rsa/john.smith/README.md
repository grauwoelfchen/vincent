# OpenVPN

THIS IS SAMPLE RSA FILES; Replace important part(s) as you need.

-----

This is your keys and certs to use OpenVPN.  
`ta.key` is needed to encrypt using _tls-auth_.

## Settings

This is your credentials and configuration.

```
USER: <USER>
RSA KEY PASSPHRASE: <USER-PASSWORD>
```

```zsh
% tree .
.
├── keys
│   └── ta.key
└── pki
    ├── ca.crt
    ├── issued
    │   └── <USER>.crt
    └── private
        └── <USER>.key

4 directories, 4 files
```

```
server: <IP-ADDRESS>
port: <PORT>
protocol: udp
client: <USER>
```

## Note

If you use GNU/Linux as your client machine, then see also following note.  
This is client side example configuration of OpenVPN on Gentoo Linux.

* https://gitlab.com/grauwoelfchen/vincent/blob/master/doc/client.md

Otherwise, you may want to download a client software from below:

* https://tunnelblick.net/


## Howto

#### GNU/Linux

;-)

#### MacOS

1. Download Tunnelblick.app from above link.
2. Double click `<NAME>.ovpn` (or drag it to tunnelblick icon on menu bar)
   and then install this configuration. This needs your system password.
3. Enter your pass phrase (and save it into key chain, if you want).
4. Choose `<NAME>` and connect to our virtual private network.
5. Go to ip address checker such as http://checkip.dyndns.com via your browser.
6. Dose it show your current ip address as `<IP-ADDRESS>`? well done!
   The install steps are success.

Plesae contact me, `<ADMIN>@<YOUR-DOMAIN>`, if you have any trouble.
