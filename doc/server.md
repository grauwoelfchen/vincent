# OpenVPN Server

## Machine

* Linode 4096 (CPU 2, 4GB Memory)
* Gentoo Linux (gentoo-20170105, with `genkernel`)

```bash
# uname -a
Linux <HOSTNAME> 4.8.6-x86_64-linode78 #1 SMP Tue Nov 1 14:51:21 EDT 2016 x86_64 Intel(R) Xeon(R) CPU E5-2697 v4 @ 2.30GHz GenuineIntel GNU/Linux
```


## Setup

See [Innocent](https://gitlab.com/grauwoelfchen/innocent) for general setup.


## OpenVPN

openvpn ebuild is moved  from `net-misc/openvpn` to `net-vpn/openvpn`.

```zsh
% sudo su
# echo 'net-vpn/openvpn iproute2' >> /etc/portage/package.use/openvpn

# exit
% sudo emerge -av openssl openvpn iproute2
```

### Create key

See [Create a public key infrastructer using the easy-rsa scripts](
https://wiki.gentoo.org/wiki/Create_a_Public_Key_Infrastructure_Using_the_easy-rsa_Scripts)
on Gentoo Wiki.

```zsh
% sudo emerge -av app-crypt/easy-rsa
% cd
% cp -a /usr/share/easy-rsa easy-rsa
```

```txt
set_var EASYRSA_DN "org"
set_var EASYRSA_REQ_COUNTRY "..."
set_var EASYRSA_REQ_PROVINCE "..."
set_var EASYRSA_REQ_CITY "..."
set_var EASYRSA_REQ_ORG "..."
set_var EASYRSA_REQ_EMAIL "..."
set_var EASYRSA_REQ_OU "..."
set_var EASYRSA_KEY_SIZE 2048
```

```
% cd /home/<USER>/easy-rsa
% ./easyrsa init-pki

Note: using Easy-RSA configuration from: ./vars

init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/<USER>/easy-rsa/pki
```

### Generate ca cert

```zsh
% cd /home/<USER>/easy-rsa
% ./easyrsa build-ca

Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
...................................+++
........+++
writing new private key to '/home/<USER>/easy-rsa/pki/private/ca.key.NNNNNNNN'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [...]:
State or Province Name (full name) [...]:
Locality Name (eg, city) [...]:
Organization Name (eg, company) [...]:
Organizational Unit Name (eg, section) [...]:
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:
Email Address [...]:

CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/<USER>/easy-rsa/pki/ca.crt
```

### Generate server request

```zsh
% cd /home/<USER>/easy-rsa
% ./easyrsa gen-req <SERVER>

Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
..............+++
..........................................................+++
writing new private key to '/home/<USER>/easy-rsa/pki/private/<SERVER>.key.NNNNNNNN'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [...]:
State or Province Name (full name) [...]:
Locality Name (eg, city) [...]:
Organization Name (eg, company) [...]:
Organizational Unit Name (eg, section) [...]:
Common Name (eg: your user, host, or server name) [...]:
Email Address [...]:

Keypair and certificate request completed. Your files are:
req: /home/<USER>/easy-rsa/pki/reqs/<SERVER>.req
key: /home/<USER>/easy-rsa/pki/private/<SERVER>.key
```

### Generate (sign request) server cert

```zsh
% cd /home/<USER>/easy-rsa
% ./easyrsa sign-req server <SERVER>

Note: using Easy-RSA configuration from: ./vars


You are about to sign the following certificate.
Please check over the details shown below for accuracy. Note that this request
has not been cryptographically verified. Please be sure it came from a trusted
source or that you have verified the request checksum with the sender.

Request subject, to be signed as a server certificate for 3650 days:

subject=
    countryName               = ...
    stateOrProvinceName       = ...
    localityName              = ...
    organizationName          = ...
    organizationalUnitName    = ...
    commonName                = ...
    emailAddress              = ...


Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes
Using configuration from /home/<USER>/easy-rsa/openssl-1.0.cnf
Enter pass phrase for /home/<USER>/easy-rsa/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
countryName           :PRINTABLE:'...'
stateOrProvinceName   :ASN.1 12:'...'
localityName          :ASN.1 12:'...'
organizationName      :ASN.1 12:'...'
organizationalUnitName:ASN.1 12:'...'
commonName            :ASN.1 12:'...'
emailAddress          :IA5STRING:'...'
Certificate is to be certified until Jan 26 11:33:33 2027 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated

Certificate created at: /home/<USER>/easy-rsa/pki/issued/<SERVER>.crt
```


### Generate DH Parameters

```zsh
% cd /home/<USER>/easy-rsa
% ./easyrsa gen-dh

Note: using Easy-RSA configuration from: ./vars
Generating DH parameters, 2048 bit long safe prime, generator 2
This is going to take a long time
...
DH parameters of size 2048 created at /home/<USER>/easy-rsa/pki/dh.pem
```

### Generate client cert

```zsh
% cd /home/<USER>/easy-rsa
% ./easyrsa build-client-full <CLIENT>

Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
........+++
........+++
writing new private key to '/home/<USER>/easy-rsa/pki/private/<CLIENT>.key.NNNNNNNN'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
Using configuration from /home/<USER>/easy-rsa/openssl-1.0.cnf
Enter pass phrase for /home/<USER>/easy-rsa/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
countryName           :PRINTABLE:'...'
stateOrProvinceName   :ASN.1 12:'...'
localityName          :ASN.1 12:'...'
organizationName      :ASN.1 12:'...'
organizationalUnitName:ASN.1 12:'...'
commonName            :ASN.1 12:'<CLIENT>'
emailAddress          :IA5STRING:'...'
Certificate is to be certified until Jan 26 10:56:16 2027 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated
```

### Generate TLS Key

```
% cd /home/<USER>/easy-rsa
% mkdir keys
% cd keys
% sudo openvpn --genkey --secret ta.key
% sudo chown <USER>:<USER> ta.key
```

### Put keys

```zsh
% sudo su
# cd /etc/openvpn/
# mkdir keys
# cd keys
# ln -s /home/<USER>/easy-rsa/pki/ca.crt ca.crt
# ln -s /home/<USER>/easy-rsa/pki/dh.pem dh.pem
# ln -s /home/<USER>/easy-rsa/keys/ta.key ta.key
# ln -s /home/<USER>/easy-rsa/pki/issued/<SERVER>.crt <SERVER>.crt
# ln -s /home/<USER>/easy-rsa/pki/private/<SERVER>.key <SERVER>.key
```

### Prepare

```zsh
% sudo $EDITOR /etc/sysctl.conf
```

```txt
net.ipv4.ip_forward = 1
```

```zsh
: for current session
# echo 1 > /proc/sys/net/ipv4/ip_forward
```

### Set Passphrase (for Certificate Private Key)

```zsh
% sudo su
# $EDITOR /root/password.ovpn
# chmod 600 /root/password.ovpn
```

```txt
# append following lines to /etc/openvpn/openvpn.conf
askpass /root/password.ovpn
auth-nocache
```

### Iptables

Create `iptables.sh` for your rules.

```zsh
% sudo su
# echo 'net-firewall/iptables conntrack nftables' >> /etc/portage/package.use/iptables
# exit
% sudo emerge -av iptables
```

```zsh
: create iptables.sh
% $EDITOR ~/iptables.sh

: use your iptables script :)
% sudo sh ./iptables.sh
```

```zsh
% sudo service iptables start
% sudo eselect rc add iptables default
```

Enable OpenVPN packets with `UDP` (e.g. `10.8.0.0/24`)

```txt
# e.g. as 10.8.0.0/24 with <PORT>
echo " * allowing traffic through openvpn (tun0)"
$IPTABLES -A INPUT -i $WAN_IF -p udp --dport <PORT> -m state --state NEW -j ACCEPT
$IPTABLES -A INPUT -i tun+ -p icmp -j ACCEPT
$IPTABLES -A FORWARD -i tun+ -o $WAN_IF -m state --state RELATED,ESTABLISHED -j ACCEPT
$IPTABLES -A FORWARD -i $WAN_IF -o tun+ -m state --state RELATED,ESTABLISHED -j ACCEPT
$IPTABLES -A FORWARD -i tun+ -o $WAN_IF -s 10.8.0.0/24 -j ACCEPT
$IPTABLES -A FORWARD -i $WAN_IF -o tun+ -s 10.8.0.0/24 -j ACCEPT
$IPTABLES -t nat -A POSTROUTING -s 10.8.0.0/24 -o $WAN_IF -j MASQUERADE

# logging
$IPTABLES -A FORWARD -m limit --limit 5/s -j LOG --log-level=1 --log-prefix '[FORWARD]: '
$IPTABLES -A FORWARD -j DROP
```

### Dnsmasq

Setup also Dnsmasq if you need it.

```zsh
% sudo su
# echo 'net-dns/dnsmasq conntrack' >> /etc/portage/package.use/dnsmasq
# exit
% sudo emerge -av dnsmasq
```

### Boot and logrotate

```zsh
% sudo service openvpn start
% sudo eselect rc add openvpn default
```

Set logrotate.

```zsh
# ls /var/log/openvpn/
openvpn.log
opennpn-status.log
```

```zsh
% sudo emerge -av logrotate
```

```txt
/var/log/openvpn* {
  missingok
  notifempty
  delaycompress
  sharedscripts
  rotate 4
  weekly
  postrotate
    test -r /run/openvpn.pid && kill -USR1 `cat /run/openvpn.pid`
  endscript
}
```


## Links

* https://wiki.gentoo.org/wiki/OpenVPN
* https://wiki.gentoo.org/wiki/Create_a_Public_Key_Infrastructure_Using_the_easy-rsa_Scripts
* http://docs.slackware.com/howtos:network_services:openvpn
* https://www.linode.com/docs/networking/vpn/secure-communications-with-openvpn-on-ubuntu-12-04-precise-and-debian-7
