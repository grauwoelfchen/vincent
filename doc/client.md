# OpenVPN Client

## Machine

* Handmade machine (CPU 4, 16GB Memory)
* Funtoo Linux

```zsh
% uname -a
Linux <HOSTNAME> 4.10.1-gentoo #21 SMP Thu Mar 2 20:12:29 JST 2017 x86_64 Intel(R) Core(TM) i5-2320 CPU @ 3.00GHz GenuineIntel GNU/Linux
```


## Setup

OpenVPN needs `CONFIG_TUN` option for kernel.

```zsh
% cd /usr/src/linux
% sudo su

: Check kernel configuration
# make menuconfig
```

```zsh
Device Drivers  --->
    [*] Network device support  --->
        [*] Network core driver support
        <*>   Universal TUN/TAP device driver support
```

```zsh
: build kernel as you like
# make && make modules_install

# cp /boot/kernel-4.10.1-gentoo /boot/.back.kernel-4.10.1-gentoo
# cp arch/x86_64/boot/bzImage /boot/kernel-4.10.1-gentoo

: I use lilo for by desktop :)
# lilo
# reboot
```

```zsh
% sudo su
# echo 'net-misc/openvpn iproute2' >> /etc/portage/package.use/openvpn
# exit
% sudo emerge -av openvpn
```


## Make config file

The init script `/etc/init.d/openvpn` supports multiple connections.

```zsh
% sudo ln -s /etc/init.d/openvpn /etc/init.d/openvpn.<NAME>
```

And then, create config file such below.

```zsh
% sudo vim /etc/openvpn/<NAME>.conf
```


## Prepare keys

```zsh
% cd ~/.openvpn
% tree .
.
├── keys
│   └── ta.key
└── pki
    ├── ca.crt
    ├── issued
    │   └── <USER>.crt
    └── private
        └── <USER>.key

4 directories, 4 files
```

## Setup logrotate

```txt
/var/log/openvpn/openvpn-XXXXX {
  missingok
  notifempty
  delaycompress
  sharedscripts
  rotate 4
  weekly
  postrotate
    test -r /run/openvpn.XXXXX.pid && kill -USR1 `cat /run/openvpn.XXXXX.pid`
  endscript
```


## Boot openvpn

```zsh
: Don't do this (symlink) :'(
: It boots too many `openvpn` scripts, because it fails
% sudo ln -s /etc/openvpn/up.sh /etc/openvpn/openvpn.XXXXX-up.sh
% sudo ln -s /etc/openvpn/down.sh /etc/openvpn/openvpn.XXXXX-down.sh
```

Just run `sudo service openvpn.XXXXX`.

```zsh
: At server
% ifconfig tun0
tun0: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 1500
        inet 10.8.0.1  netmask 255.255.255.255  destination 10.8.0.2
        inet6 fe80::8fb0:c2b9:9be:1f84  prefixlen 64  scopeid 0x20<link>
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 100  (UNSPEC)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3  bytes 144 (144.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

: at client
% ifconfig tun0
tun0: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 1400
        inet 10.8.0.50  netmask 255.255.255.255  destination 10.8.0.49
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 100  (UNSPEC)
        RX packets 3468  bytes 1292573 (1.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3828  bytes 469359 (458.3 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```
