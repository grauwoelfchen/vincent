# Vincent

`OpenVPN` Setup Notes on Linode using Gentoo Linux.


## Pages

* [doc/server.md](/doc/server.md)
* [doc/client.md](/doc/client.md)


## Memo

* An example of `openvpn.conf` (server) is [here](
  https://wiki.gentoo.org/wiki/OpenVPN#Configuration) (Gentoo Wiki).
* OpenVPN Client for iOS seems that it does not support `fragment` option.
